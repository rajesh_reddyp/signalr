﻿using Owin;
using Microsoft.Owin;
using PersistantConnection.App_Start;
using Connections;
[assembly:OwinStartup(typeof(Startup))]
namespace PersistantConnection.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app) {
            app.MapSignalR<ChatConnection>("/chat");
        }
    }
}