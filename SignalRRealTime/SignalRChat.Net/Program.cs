﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Client;
using System.Net;

namespace SignalRChat.Net
{
    class Program
    {
        static void Main(string[] args)
        {
            // persistance connection
            //var connection = new Connection("http://localhost:4906/chat");
            //connection.Received += connection_Received;
            //connection.Start().ContinueWith(t => Console.WriteLine("Connected")).Wait();
            //var line = string.Empty;
            //while ((line = Console.ReadLine()) != null) {
            //    connection.Send(line).Wait();
            //}
            
            // Hubs
            var hubconnection = new HubConnection("http://localhost:6033");
            hubconnection.Credentials = CredentialCache.DefaultNetworkCredentials;
            var chat = hubconnection.CreateHubProxy("chat");
            chat.On("addMessage", message => Console.WriteLine("Received:" + message));
            hubconnection.Start().Wait();
            var line = string.Empty;
            while ((line = Console.ReadLine()) != null)
            {
                chat.Invoke("Send", line).Wait();
            }
        }

        static void connection_Received(string obj)
        {
            Console.WriteLine(obj.ToString());
        }
    }
}
