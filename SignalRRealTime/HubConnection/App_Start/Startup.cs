﻿using Owin;
using Microsoft.Owin;
using HubConnection.Hubs;
using HubConnection.App_Start;
[assembly:OwinStartup(typeof(Startup))]

namespace HubConnection.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app) {
            app.MapSignalR();
        }

    }
}