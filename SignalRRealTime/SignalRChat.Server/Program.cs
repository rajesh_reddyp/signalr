﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin.Hosting;

namespace SignalRChat.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            var url = "http://localhost:8080";
            using (WebApp.Start<Startup>(url)) {
                Console.WriteLine("Server running on {0}",url);
                Console.ReadLine();
            }            
            
        }
    }
}
