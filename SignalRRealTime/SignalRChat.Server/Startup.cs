﻿using Microsoft.AspNet.SignalR;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SignalRChat.Server
{
    public class Startup
    {
        public void Configuration(IAppBuilder builder) {
            builder.Use(typeof(SecurityInspectionHandler));
            var hubConfiguration = new HubConfiguration() { 
               EnableCrossDomain=true, EnableJavaScriptProxies=true, EnableDetailedErrors=true
            };
            builder.MapHubs(hubConfiguration);
        }
    }
}
