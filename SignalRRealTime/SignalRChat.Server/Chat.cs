﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubConnection.Hubs
{
    
    public class Chat:Hub
    {
        public void Send(string message) { 
          Clients.All.addMessage(message);
        }

        public void CreateChatRoom(string room) {
            if (!ChatRooms.Exists(room))
            {
                ChatRooms.Add(room);
                Clients.All.addChatRoom(room);
            } 
        }

        public void Join(string room) {
            Groups.Add(Context.ConnectionId, room);
        }
        public override System.Threading.Tasks.Task OnConnected()
        {
            foreach (var room in ChatRooms.GetAll())
                Clients.Caller.addChatRoom(room);
                return base.OnConnected();
        }
    }

}